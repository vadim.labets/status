<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header id="masthead" class="site-header" role="banner">
    <div class="header">
        <div class="container row flex-justify-space-between">
            <div class="logo">
                <a href="<?php echo site_url(); ?>">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png">
                </a>
            </div>
            <div class="header-page-switcher_box">
                <?php get_template_part( 'template-parts/page-switcher' ); ?>
            </div>
        </div>
    </div>
</header>

<div id="content" class="site-content">
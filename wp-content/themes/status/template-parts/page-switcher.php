<?php

global $post;

if ( ! is_object( $post ) ) return;
$current_page_title = get_the_title( $post->ID );

$pages = get_posts([
    'post_type' => 'page',
    'post_status' => 'publish',
    'posts_per_page' => -1
]);

$select = '<div class="header-page-switcher_select">';
foreach ( $pages as $p ) {
    $class = ( $p->post_title == $current_page_title ) ? ' active' : '';
    $select .= '<a href="' . get_permalink( $p->ID ) . '" class="page-select-item ' . $class . '">' . $p->post_title . '</a>';
}
$select .= '</div>';

?>

<div class="header-page-switcher">
    <div class="header-page-switcher_title"><?php echo $p->post_title; ?> </div>
    <?php echo $select; ?>
</div>

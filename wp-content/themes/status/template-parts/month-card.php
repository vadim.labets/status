<?php

use StatusTheme\Singleton as Singleton;
use StatusTheme\SingletonOption as PingdomOpion;
use StatusTheme\PingdomAdmin as PingdomAdmin;
use StatusTheme\pingdomCalendar as pingdomCalendar;

$analysis = PingdomOpion::getInstance( PingdomAdmin::$pingdome_analysis_field_name, [] );

$month = $args['month'];
$year = $args['year'];
$first_day = date("N", mktime(0, 0, 0, $month, 1, $year));
$days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
$offset = $first_day - 1;
$day_count = 1;
$dateFormatter = new IntlDateFormatter('en_US', IntlDateFormatter::LONG, IntlDateFormatter::NONE);
$dateFormatter->setPattern('MMMM y');

$PingdomAdmin = Singleton::getInstance('StatusTheme\PingdomAdmin');
$checks = PingdomOpion::getInstance( $PingdomAdmin::$pingdome_checks_field_name );
$first_month_second =  mktime(0, 0, 0, $month, 1, $year);
$first_month_day = date( 'Y-m-d', $first_month_second );
$last_month_second = mktime(23, 59, 59, $month + 1, 0, $year);
$up_down_data = $PingdomAdmin->get_summary_up_time( $checks, $first_month_second, $last_month_second );

?>

<div class="month-card">
    <div class="month-data">
        <div class="month-calendar-data"><?php echo $dateFormatter->format( new DateTime( $first_month_day ) ); ?></div>
        <div class="month-uptime">
            <span><?php echo round( $up_down_data['up']/($up_down_data['up']+$up_down_data['down']), 4 )*100; ?>%</span>
        </div>
    </div>
    <div class="month-box">
        <div class="week-days">
            <?php
            for ($i = 0; $i < 7; $i++) {
                $date = new DateTime();
                $date->modify("+$i day");
                echo "<div class='week-day'>" . IntlDateFormatter::create('en_ES', IntlDateFormatter::FULL, IntlDateFormatter::NONE, NULL, NULL, 'ccc')->format( $date ) . "</div>";
            }
            ?>
        </div>
        <div class="month-days">
            <?php
            for ($i = 0; $i < $offset; $i++) {
                echo '<div class="month-day empty"></div>';
            }
            while ($day_count <= $days_in_month) {
                $day_first_second = mktime(0, 0, 0, $month, $day_count, $year);
                $incidents = pingdomCalendar::get_day_incidents( $day_first_second, $analysis );
                $class = ( count( $incidents ) > 0 ) ? 'has-incidents' : '';
                ?>
                <div class="month-day <?php echo $class; ?>">
                    <?php
                    echo $day_count;
                    if ( ! empty( $class ) ) {
                        get_template_part( 'template-parts/incident-card', null, [ 'incidents' => $incidents, 'day_tirst_second' => $day_first_second ] );
                    }
                    ?>
                </div>
                <?php
                $day_count++;
                $offset++;
            }
            ?>
        </div>
    </div>
</div>

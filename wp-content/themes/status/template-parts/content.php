<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Status
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php if ( is_singular() ) : ?>
            <h1 class="entry-title"><?php the_title(); ?></h1>
        <?php else : ?>
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
        <?php endif; ?>
    </header><!-- .entry-header -->

    <div class="entry-content">
        <?php
        the_content();

        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'status' ),
            'after'  => '</div>',
        ) );
        ?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
        <div class="entry-meta">
            <span class="posted-on"><?php echo esc_html__( 'Posted on', 'status' ); ?> <?php the_date(); ?></span>
            <span class="byline"><?php echo esc_html__( 'by', 'status' ); ?> <?php the_author(); ?></span>
        </div><!-- .entry-meta -->
    </footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
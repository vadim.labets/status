<?php

use StatusTheme\Singleton as Singleton;
use StatusTheme\SingletonOption as PingdomOpion;
use StatusTheme\PingdomAdmin as PingdomAdmin;
use StatusTheme\pingdomCalendar as pingdomCalendar;

$incidents = $args['incidents'];
$day_tirst_second = $args['day_tirst_second'];

$outages = PingdomOpion::getInstance( PingdomAdmin::$pingdome_outages_field_name );
$dateFormatter = new IntlDateFormatter('en_US', IntlDateFormatter::LONG, IntlDateFormatter::NONE);
$dateFormatter->setPattern('MMMM dd y');

?>

<div class="day-incidents">
    <div class="date">
        <?php
            echo $dateFormatter->format( new DateTime( date( 'Y-m-d', $day_tirst_second ) ) );
            $dateFormatter->setPattern('MMMM dd y HH:mm:ss');
        ?>
    </div>
    <div class="incidents">
        <?php foreach( $incidents as $check_id => $incident_array ) :
            foreach ( $incident_array as $incident ) :
                $post = get_posts([
                    'post_type' => 'incident',
                    'post_status' => 'publish',
                    'posts_per_page' => 1,
                    'meta_query' => [
                        [
                            'key' => 'status_connected_incident',
                            'value' => $incident['id'],
                            'compare' => '='
                        ]
                    ]
                ]);
                if ( count( $post ) > 0 ) {
                    $p = $post[0];
                    $blocks = parse_blocks( $p->post_content );
                    foreach ( $blocks as $block ) {
                        if ( isset( $block['blockName'] ) && $block['blockName'] === 'status/post' ) {
                            $attributes = $block['attrs'];
                            $incident['custom_description'] = $attributes['incidentCustomDescription'];
                            break;
                        }
                    }
                }
                $outage_data = pingdomCalendar::get_incident_outage( $incident, $check_id, $outages );
            ?>
                <div class="incident">
                    <div class="incident-host"><?php echo $incident['host_name']; ?></div>
                    <div class="incident-description"><?php echo ( ! empty( $incident['custom_description'] )  ? $incident['custom_description'] : $incident['longdesc'] ); ?></div>
                    <div class="incident-timing">
                        <div class="incident-time-start"><?php echo $dateFormatter->format( new DateTime( date( 'Y-m-d H:i:s', $outage_data->timefrom ) ) ); ?></div>
                        <div class="incident-time-end"><?php echo $dateFormatter->format( new DateTime( date( 'Y-m-d H:i:s', $outage_data->timeto ) ) ); ?></div>
                    </div>
                </div>
            <?php
                endforeach;
            ?>
        <?php endforeach; ?>
    </div>
</div>

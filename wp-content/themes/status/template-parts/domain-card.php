<?php

$check = $args['check'];

switch ( $check->status ) {
    case 'up' :
        $status = 'up';
        $icon = 'small-round-chevron';
        break;
    case 'down' :
        $status = 'downd';
        $icon = 'stop';
        break;
    default :
        $status = 'warning';
        $icon = 'warning';
}
$status_descriptions = [
  'up' => __( 'No issues', 'status' ),
  'down' => __( 'Domain is down', 'status' ),
  'warning' => __( 'Incident', 'status' )
];

?>

<div class="dashboard-content__card status-<?php echo $status; ?>">
    <div class="dashboard-content__card-content-box">
        <div class="dashboard-content__card-header flex-justify-space-between flex-align-center">
            <div class="dashboard-content__card-header_content">
                <svg class="icon <?php echo $icon; ?>">
                    <use xlink:href="#<?php echo $icon; ?>"></use>
                </svg>
                <span><?php echo $check->hostname; ?></span>
            </div>
            <svg class="icon small-chevron">
                <use xlink:href="#small-chevron"></use>
            </svg>
        </div>
        <div class="dashboard-content__card-content">
            <?php echo $status_descriptions[ $status ]; ?>
        </div>
    </div>
</div>

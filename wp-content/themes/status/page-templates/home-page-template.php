<?php

use StatusTheme\Singleton as Singleton;
use StatusTheme\SingletonOption as PingdomOpion;

/* Template Name: Home Page */

$current_time = time();
$PingdomAdmin = Singleton::getInstance('StatusTheme\PingdomAdmin');
$checks = PingdomOpion::getInstance( $PingdomAdmin::$pingdome_checks_field_name );
$outages = PingdomOpion::getInstance( $PingdomAdmin::$pingdome_outages_field_name );
if ( $current_time - $checks['last_updated'] > 60 * 10 ) {
    $checks = $PingdomAdmin->update_checks( 'return' );
}

$up_down_data = $PingdomAdmin->get_summary_up_time( $checks, strtotime('-90 days', time()), time() );

get_header();

?>

<main id="primary" class="site-main">
    <div class="container flex-column">
        <div class="dashboard-header">
            <svg class="icon large-round-chevron">
                <use xlink:href="#large-round-chevron"></use>
            </svg>
            <h2><?php echo __( 'OpenUp is up and running', 'status' ); ?></h2>
            <div class="dashboard-header__description">
                <span><?php echo __( 'Having trouble?', 'status' ); ?></span>
                <a href="#"><?php echo __( 'Please let us know', 'status' ); ?></a>
            </div>
        </div>
        <div class="dashboard-content">
            <div class="dashboard-content__title"><?php echo __( 'Current website status', 'status' ); ?></div>
            <div class="dashboard-content__cards">
                <?php
                    foreach( $checks as $check ) {
                        if ( is_object( $check ) ) {
                            get_template_part( 'template-parts/domain-card', null, [ 'check' => $check ] );
                        }
                    }
                ?>
            </div>
        </div>
    </div>
    <div class="uptime-section">
        <div class="uptime-section__content container">
            <div class="uptime-section__content-value">
                <?php echo __( 'Uptime for the current quarter:', 'status' ) . ' '; ?>
                <span><?php echo round( $up_down_data['up']/($up_down_data['up']+$up_down_data['down']), 4 )*100; ?>%</span>
            </div>
            <a class="btn medium" href="/calendar"><?php echo __( 'See history', 'status' ); ?></a>
        </div>
    </div>
</main>

<?php
    get_footer();
?>

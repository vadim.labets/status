<?php

use StatusTheme\pingdomCalendar as pingdomCalendar;

/* Template Name: Calendar Page */

$prev_months = pingdomCalendar::get_previous_months();

get_header();

?>

<div clASS="container">
    <div class="calendar">
        <?php
        foreach( $prev_months as $month_data ) {
            get_template_part( 'template-parts/month-card', null, [ 'year' => $month_data['year'], 'month' => $month_data['month'] ] );
        }
        ?>
    </div>
</div>

<?php

get_footer();



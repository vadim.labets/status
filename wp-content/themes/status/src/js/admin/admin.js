import { createIncidentBlock } from './blocks/post/index';

document.addEventListener('DOMContentLoaded', function() {

    const ajaxStartEvent = new CustomEvent('ajaxStart', { detail: {} });
    const ajaxFinishEvent = new CustomEvent('ajaxFinish', { detail: {} });

    document.querySelectorAll('.pingdom-admin-content-tab .pingdom-admin-button').forEach(function (button) {
        button.addEventListener('click', function (e) {
            e.preventDefault();
            let requestData = {};
            Array.from(this.attributes).forEach(function (attribute) {
                if (attribute.nodeName.startsWith('data-value_')) {
                    let attributeName = attribute.nodeName.replace('data-value_', '');
                    requestData[attributeName] = attribute.nodeValue;
                }
            });
            document.dispatchEvent(ajaxStartEvent);
            const xhr = new XMLHttpRequest();
            xhr.open('POST', ajaxurl, true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        const response = JSON.parse(xhr.responseText)
                        const parentDiv = button.closest('.pingdom-admin-content');
                        const actionResultDiv = parentDiv.querySelector('.pingdom-admin-action-result');
                        if (actionResultDiv) {
                            actionResultDiv.innerHTML = response.message;
                        }
                    } else {
                        console.error(xhr.responseText);
                    }
                    document.dispatchEvent(ajaxFinishEvent);
                }
            };
            const formData = Object.keys(requestData).map(function (key) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(requestData[key]);
            }).join('&');
            xhr.send(formData);
        });
    });

    const tokenInput = document.getElementById('pingdom-token');
    if (tokenInput) {
        tokenInput.addEventListener('change', function(e) {
            const pingdomSettingsForm = tokenInput.closest('#pingdom-settings-form');
            const pingdomAdminButton = pingdomSettingsForm.querySelector('.pingdom-admin-button');
            pingdomAdminButton.setAttribute('data-value_pingdom_token', e.target.value);
        });
    }

    createIncidentBlock();

});
import { __ } from '@wordpress/i18n';
import blockMeta from './block.json';
import { registerPlugin } from '@wordpress/plugins';
import { PanelBody } from '@wordpress/components';
import { PluginDocumentSettingPanel } from '@wordpress/edit-post';
import { withSelect, withDispatch } from '@wordpress/data';
import { useBlockProps, RichText } from '@wordpress/block-editor';
import { registerBlockType } from '@wordpress/blocks';
import { useEntityProp } from '@wordpress/core-data';
import { useSelect } from '@wordpress/data';
import { SelectControl, TextControl } from '@wordpress/components';
import { useState, useEffect } from '@wordpress/element';
import apiFetch from '@wordpress/api-fetch';
const moment = require('moment');

const IncidentDataBlock = ( props ) => {
    const blockProps = useBlockProps( {
        className: 'status-post-block',
    } );
    const postType = useSelect(
        ( select ) => select( 'core/editor' ).getCurrentPostType(),
        []
    );
    const { attributes, setAttributes } = props;
    const [incidentId, setIncidentId] = useState(attributes.incidentId || '');
    const [incidentDomain, setIncidentDomain] = useState(attributes.incidentDomain || '');
    const [incidentDescription, setIncidentDescription] = useState(attributes.incidentDescription || '');
    const [incidentStartDate, setIncidentStartDate] = useState(attributes.incidentStartDate || '');
    const [incidentEndDate, setIncidentEndDate] = useState(attributes.incidentEndDate || '');
    const [incidentCustomDescription, setIncidentCustomDescription] = useState(attributes.incidentCustomDescription || '');
    const [incidentOptions, setIncidentOptions] = useState(attributes.incidentOptions || []);
    const [incidents, setIncidents] = useState(attributes.incidents || null);
    const [isLoading, setIsLoading] = useState(true);

    const [ meta, setMeta ] = useEntityProp( 'postType', postType, 'meta' );
    const connectedIncident = meta ? meta[ 'status_connected_incident' ] : '';
    const connectedCheckId = meta ? meta[ 'status_connected_check_id' ] : '';

    useEffect(() => {
        fetchIncidentData();
    }, [incidentId]);

    const fetchIncidentData = () => {
        apiFetch({ path: '/pingdom/v1/incidents' })
            .then((incidents) => {
                const emptyIncident = { '0' : {
                        domain: __('Empty', 'status'),
                        description: __('Empty', 'status'),
                        time_start: '',
                        time_end: '',
                        check_id: ''
                    }
                };
                const newData = { ...emptyIncident, ...incidents.data };
                const options = [
                    { value: '0', label: __('Empty', 'status') },
                    ...Object.entries(incidents.data).map(([key, incident]) => ({
                        value: key,
                        label: `${incident.domain}, ${incident.description}`
                    }))
                ];
                setIncidents(newData);
                setIncidentOptions(options);
                setIsLoading(false);

            })
            .catch((error) => {
                console.error('Error fetching incident data:', error);
                setIsLoading(false);
            });
    };

    return (
        <div { ...blockProps }>
            {isLoading ? (
                <p>{__('Loading...', 'status')}</p>
            ) : (
                <>
                    <SelectControl
                        label={__('Incident ID', 'status')}
                        value={incidentId}
                        options={incidentOptions.map(option => ({
                            value: option.value,
                            label: option.label
                        }))}
                        onChange={value => {
                            setIncidentId(value);
                            const selectedIncident = incidents[value];
                            setIncidentDomain(selectedIncident.domain);
                            setIncidentDescription(selectedIncident.description);
                            setIncidentStartDate(selectedIncident.time_start);
                            setIncidentEndDate(selectedIncident.time_end);

                            setAttributes({
                                incidentId: value,
                                incidentDomain: selectedIncident.domain,
                                incidentDescription: selectedIncident.description,
                                incidentStartDate: selectedIncident.time_start,
                                incidentEndDate: selectedIncident.time_end,
                            });

                            const updatedMeta = {
                                ...meta,
                                status_connected_incident: value,
                                status_connected_check_id: selectedIncident.check_id
                            };
                            setMeta( updatedMeta );

                        }}
                    />
                    <TextControl
                        label={__('Incident Domain', 'status')}
                        value={incidentDomain}
                        onChange={value => setAttributes({ incidentDomain: value })}
                    />
                    <TextControl
                        label={__('Incident Description', 'status')}
                        value={incidentDescription}
                        onChange={value => setAttributes({ incidentDescription: value })}
                    />
                    <TextControl
                        label={__('Incident Start Date', 'status')}
                        value={moment.unix(incidentStartDate).utc().format('DD.MM.YYYY HH:mm:ss')}
                        onChange={value => setAttributes({ incidentStartDate: value })}
                    />
                    <TextControl
                        label={__('Incident End Date', 'status')}
                        value={moment.unix(incidentEndDate).utc().format('DD.MM.YYYY HH:mm:ss')}
                        onChange={value => setAttributes({ incidentEndDate: value })}
                    />
                    <RichText
                        label={__('Incident Custom Description', 'status')}
                        value={incidentCustomDescription}
                        onChange={value => setAttributes({ incidentCustomDescription: value })}
                        wrapperClassName="incident-custom-description-class"
                        placeholder={__('Custom Description', 'status')}
                    />
                </>
            )}
        </div>
    );
};


const createIncidentBlock = () => {
    const checkIncidentPostType = () => {
        return (document.body.classList.contains('post-type-incident'));
    };

    const handleIncidentBlock = () => {
        if (checkIncidentPostType()) {
            registerBlockType(blockMeta, {
                title: __('Incident Data Block', 'status'),
                icon: 'shield',
                category: 'common',
                edit: (props) => {
                    return (
                        <div>
                            <IncidentDataBlock {...props} />;
                            <MetaPanelWithDispatch/>
                        </div>
                    )
                },
                save: (props) => {
                    const {attributes} = props;
                    const {
                        incidentId,
                        incidentDomain,
                        incidentDescription,
                        incidentStartDate,
                        incidentEndDate,
                        incidentCustomDescription
                    } = attributes;
                    const postId = wp.data.select('core/editor').getCurrentPostId();
                    const meta = wp.data.select('core/editor').getEditedPostAttribute('meta');
                    if (meta !== undefined) {
                        const statusConnectedIncident = meta['status_connected_incident'];
                        const statusConnectedCheckId = meta['status_connected_check_id'];
                    }

                    return (
                        <div>
                        </div>
                    );
                },
            });

            const MetaPanel = ({meta, onUpdateMeta}) => {

                const meta_connected_incident = meta && meta.status_connected_incident ? meta.status_connected_incident : '';
                const meta_connected_check_id = meta && meta.status_connected_check_id ? meta.status_connected_check_id : '';

                return (
                    <PluginDocumentSettingPanel
                        name="incident-meta-panel"
                        title="Incident Meta"
                        className="incident-meta-panel"
                    >
                        <PanelBody>
                            <TextControl
                                label="Post Connected Incident"
                                value={meta_connected_incident}
                                onChange={(value) => onUpdateMeta({status_connected_incident: value})}
                            />
                            <TextControl
                                label="Post Connected Check ID"
                                value={meta_connected_check_id}
                                onChange={(value) => onUpdateMeta({status_connected_check_id: value})}
                            />
                        </PanelBody>
                    </PluginDocumentSettingPanel>
                );
            };

            const MetaPanelWithSelect = withSelect((select) => {
                return {
                    meta: select('core/editor').getEditedPostAttribute('meta'),
                };
            })(MetaPanel);

            const MetaPanelWithDispatch = withDispatch((dispatch) => {
                return {
                    onUpdateMeta: (newMeta) => {
                        dispatch('core/editor').editPost({meta: newMeta});
                    },
                };
            })(MetaPanelWithSelect);

        }
    }

    handleIncidentBlock();

}

export { createIncidentBlock };
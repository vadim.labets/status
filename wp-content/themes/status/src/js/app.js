document.addEventListener('DOMContentLoaded', function() {

    var cards = document.querySelectorAll('.dashboard-content__card');

    cards.forEach(function(card) {
        var headerContent = card.querySelector('.dashboard-content__card-header');
        var cardContent = card.querySelector('.dashboard-content__card-content');

        headerContent.addEventListener('click', function() {
            card.classList.toggle('opened');

            if (card.classList.contains('opened')) {
                cardContent.style.height = cardContent.scrollHeight + 'px';
            } else {
                cardContent.style.height = '0';
            }
        });
    });

});
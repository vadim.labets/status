</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="footer container row flex-justify-space-between">
        <div class="footer-date"><?php echo 'OpenUp © ' . date('Y'); ?></div>
        <div class="footer-socials">
            <svg class="icon linkedin">
                <use xlink:href="#linkedin"></use>
            </svg>
            <svg class="icon instagram">
                <use xlink:href="#instagram"></use>
            </svg>
        </div>
    </div>
</footer>
<?php get_template_part('template-parts/svg/svg-icon'); ?>
<?php wp_footer(); ?>
</body>
</html>
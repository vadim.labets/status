<?php

namespace StatusTheme;

use StatusTheme\Singleton as Singleton;
use StatusTheme\SingletonOption as PingdomOpion;

class pingdomCalendar {

    public static function get_previous_months() {
        $current_month = date("n");
        $current_year = date("Y");

        $previous_months = array();

        $previous_months[] = array('month' => $current_month, 'year' => $current_year);

        for ($i = 1; $i <= 11; $i++) {
            $prev_month = $current_month - $i;

            if ($prev_month < 1) {
                $prev_month += 12;
                $prev_year = $current_year - 1;
            } else {
                $prev_year = $current_year;
            }

            $previous_months[] = array('month' => $prev_month, 'year' => $prev_year);
        }

        return $previous_months;

    }


    public static function form_incident( $analis_id, $analis_data, $check_id ) {

        $PingdomAdmin = Singleton::getInstance('StatusTheme\PingdomAdmin');
        $checks = PingdomOpion::getInstance($PingdomAdmin::$pingdome_checks_field_name);

        $incident = [];
        $data = $analis_data->data;
        $incident['id'] = $analis_id;
        $incident['data'] = $data;
        $incident['desc'] = $data->firststatusdesc;
        $incident['longdesc'] = $data->firststatusdesclong;
        $incident['time_start'] = $analis_data->timefirsttest;
        $incident['time_end'] = $analis_data->timeconfirmtest;
        $incident['host_name'] = $checks[ $check_id ]->hostname;

        return $incident;

    }


    public static function get_day_incidents( $day_first_second, $analysis ) {

        $day_last_second = $day_first_second + 60 * 60 * 24;
        $incidents = [];

        foreach ( $analysis as $check_id => $analysis_data ) {
            foreach ( $analysis_data as $analis_id => $analis_data ) {
                if ( $analis_data->timefirsttest >= $day_first_second && $analis_data->timefirsttest <= $day_last_second ) {
                    $incident = self::form_incident( $analis_id, $analis_data, $check_id );
                    $incidents[ $check_id ][] = $incident;
                }
            }
        }

        return $incidents;

    }


    public static function get_incident_outage( $incident, $check_id, $outages ) {

        $result = new \stdClass();
        $result->timeto = $result->timefrom = 0;

        if ( isset( $outages[ $check_id ] ) ) {

            foreach( $outages[ $check_id ] as $outage ) {
                if ( $outage->status != 'up' ) {
                    $intersection = pingdomCalendar::get_diapason_intersection(
                        ['from' => $incident['time_start'], 'to'=> $incident['time_end']],
                        ['from' => $outage->timefrom, 'to'=> $outage->timeto],
                    );
                    if ( $intersection != 0 ) {
                        $result = $outage;
                    }
                }
            }

        }

        return $result;

    }

    public static function get_diapason_intersection( $d1 = [], $d2 = [] ) {

        if ( $d1['to'] >= $d2['from'] && $d1['to'] <= $d2['to'] && $d1['from'] <= $d2['from'] ) {
            $value = $d1['to'] - $d2['from'];
        } elseif ( $d1['to'] >= $d2['to'] && $d1['from'] >= $d2['from'] && $d1['from'] <= $d2['to'] ) {
            $value = $d2['to'] - $d1['from'];
        } elseif ( $d1['from'] >= $d2['from'] && $d1['to'] <= $d2['to'] ) {
            $value = $d1['to'] - $d1['from'];
        } else {
            $value = 0;
        }

        return $value;

    }

}
<?php

namespace StatusTheme;

defined('ABSPATH') || exit;

class SingletonOption extends Singleton {

    public static function getInstance( $optionName, $default_value = '' ) {
        if ( ! isset( self::$instances[ $optionName ] ) ) {
            $option = get_option( $optionName );
            if ( ! empty( $option ) ) {
                self::$instances[ $optionName ] = $option;
            }
            else {
                return $default_value;
            }
        }
        return self::$instances[ $optionName ];
    }

}
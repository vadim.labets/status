<?php

namespace StatusTheme;

defined('ABSPATH') || exit;

class Status {

    function __construct() {

        add_action( 'init', function() {
            add_action( 'admin_enqueue_scripts', [$this, 'admin_enqueue_scripts'] );
            add_action( 'wp_enqueue_scripts', [$this, 'enqueue_scripts'] );
            //add_filter( 'allowed_block_types_all', [$this, 'exclude_block_for_post_types'], INF, 2 );
            $this->register_incident_post_type();
            $this->register_incident_taxonomy();
            $this->register_meta_fields();
        } );

    }

    public function admin_enqueue_scripts() {

        wp_enqueue_style('status-main-styles', get_template_directory_uri() . '/build/css/style-admin.css', array(), wp_get_theme()->get('Version'));
        wp_enqueue_script('status-scripts', get_stylesheet_directory_uri() . '/build/js/admin.js', array('jquery'), wp_get_theme()->get('Version'), true);

    }

    public function enqueue_scripts() {

        wp_enqueue_style('status-default-styles', get_stylesheet_uri(), array(), wp_get_theme()->get('Version'));
        wp_enqueue_style('status-main-styles', get_template_directory_uri() . '/build/css/style-app.css', array(), wp_get_theme()->get('Version'));
        wp_enqueue_script('status-scripts', get_stylesheet_directory_uri() . '/build/js/app.js', array('jquery'), wp_get_theme()->get('Version'), true);

    }

    public function register_meta_fields() {

        register_post_meta( 'incident', 'status_connected_incident', array(
            'show_in_rest' => true,
            'single' => true,
            'type' => 'string',
        ) );

        register_post_meta( 'incident', 'status_connected_check_id', array(
            'show_in_rest' => true,
            'single' => true,
            'type' => 'integer',
        ) );

    }

    public function exclude_block_for_post_types( $allowed_blocks, $editor_context ) {

        if ( ! in_array( $editor_context->post->post_type, ['post'] ) ) {
            $blocks =  \WP_Block_Type_Registry::get_instance()->get_all_registered();
            $blocks_keys = array_keys( $blocks );
            $allowed_blocks = array_diff( $blocks_keys, array( 'status/post' ) );
        }

        return $allowed_blocks;

    }


    public function post_type_labels($singular = 'Post', $plural = 'Posts') {
        $p_lower = strtolower($plural);
        $s_lower = strtolower($singular);

        return [
            'name'                   => $plural,
            'singular_name'          => $singular,
            'add_new_item'           => "New $singular",
            'edit_item'              => "Edit $singular",
            'view_item'              => "View $singular",
            'view_items'             => "View $plural",
            'search_items'           => "Search $plural",
            'not_found'              => "No $p_lower found",
            'not_found_in_trash'     => "No $p_lower found in trash",
            'parent_item_colon'      => "Parent $singular",
            'all_items'              => "All $plural",
            'archives'               => "$singular Archives",
            'attributes'             => "$singular Attributes",
            'insert_into_item'       => "Insert into $s_lower",
            'uploaded_to_this_item'  => "Uploaded to this $s_lower",
        ];
    }


    public function register_incident_post_type() {

        $labels = $this->post_type_labels('Incident', 'Insidents');

        $args = array(
            'description' => __('Incidents', 'status'),
            'labels' => $labels,
            'supports' => ['title', 'editor', 'thumbnail', 'revisions', 'custom-fields'],
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_icon'     => 'dashicons-text',
            'rewrite' => ['slug' => 'incidents', 'with_front' => false],
            'can_export' => true,
            'has_archive' => false,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type'    => 'post',
            'show_in_rest' => true,
            'taxonomies' => array('incidents_tax'),
            'template' => array(
                array('status/post', array(
                    'lock' => array(
                        'move' => false,
                        'remove' => false,
                    ),
                    'mode' => 'edit',
                )),
            ),
        );

        register_post_type('incident', $args);

    }

    public function register_incident_taxonomy() {

        $tax =  'incidents_tax';
        $type = ['incident'];

        $labels = $this->post_type_labels('Incident Terms', 'Incident Terms');

        $args = [
            'description'         => 'Incident Terms',
            'labels'              => $labels,
            'hierarchical'        => true,
            'show_ui'             => true,
            'show_admin_column'   => true,
            'show_in_quick_edit'  => true,
            'show_in_menu'        => true,
            'rewrite'             => ['slug' => 'incidents-tax', 'with_front' => false],
        ];

        register_taxonomy($tax, $type, $args);

    }

}



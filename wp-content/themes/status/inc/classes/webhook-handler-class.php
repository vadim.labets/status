<?php

use StatusTheme\Singleton as Singleton;
use StatusTheme\SingletonOption as PingdomOpion;
use StatusTheme\pingdomCalendar as pingdomCalendar;

class Webhook_Handler {

    public function __construct() {
        add_action( 'rest_api_init', array( $this, 'register_webhook_endpoints' ) );
    }

    public function register_webhook_endpoints() {

        register_rest_route( 'pingdom/v1', '/webhook', array(
            'methods'  => 'POST',
            'callback' => array( $this, 'handle_webhook_request' ),
        ) );

        register_rest_route( 'pingdom/v1', '/incidents', array(
            'methods'  => 'GET',
            'callback' => array( $this, 'getIncidents' ),
        ) );

    }

    public function handle_webhook_request( WP_REST_Request $request ) {

        $body = $request->get_body();
        $data = json_decode( $body, true );
        if ( isset( $data['check_id'] ) ) {
            $PingdomAdmin = Singleton::getInstance('StatusTheme\PingdomAdmin');
            $checks = PingdomOpion::getInstance($PingdomAdmin::$pingdome_checks_field_name);
            if ( isset( $checks[ $data['check_id'] ] ) ) {
                $PingdomAdmin->update_checks('return');
                $PingdomAdmin->update_checks_analysis( [ $data->check_id ], [ 'from' => strtotime('-1 days', time()), 'to' => time() ] );
                if ( $data['previous_state'] == 'UP' && $data['current_state'] == 'DOWN' ) {
                    $PingdomAdmin->create_incidents_posts( 'return' );
                }
                if ( $data['previous_state'] == 'DOWN' && $data['current_state'] == 'UP' ) {
                    $PingdomAdmin->update_checks_outage( [ $data->check_id, [ 'from' => strtotime('-1 days', time()), 'to' => time() ] ] );
                }
                return new WP_REST_Response( array( 'success' => true ), 200 );
            }
        }
        else {
            return new WP_REST_Response( array( 'success' => false ), 403 );
        }
    }

    public function getIncidents() {

        $data = [];
        $PingdomAdmin = Singleton::getInstance('StatusTheme\PingdomAdmin');
        $checks = PingdomOpion::getInstance($PingdomAdmin::$pingdome_checks_field_name);
        $analysis = PingdomOpion::getInstance($PingdomAdmin::$pingdome_analysis_field_name);
        $outages = PingdomOpion::getInstance( $PingdomAdmin::$pingdome_outages_field_name );
        foreach ( $analysis as $check_id => $incidents ) {
            $check = $checks[ $check_id ];
            foreach ( $incidents as $incident_id => $incident ) {
                $incident_data = [
                  'time_start' => $incident->timefirsttest,
                  'time_end' => $incident->timeconfirmtest
                ];
                $outage_data = pingdomCalendar::get_incident_outage( $incident_data, $check_id, $outages );
                $data[ $incident_id ] = [
                    'check_id' => $check_id,
                    'domain' => $check->hostname,
                    'description' => $incident->data->firststatusdesclong,
                    'time_start' => $outage_data->timefrom,
                    'time_end' => $outage_data->timeto
                ];
            }
        }

        return new WP_REST_Response( array( 'data' => $data ), 200 );

    }

}

new Webhook_Handler();

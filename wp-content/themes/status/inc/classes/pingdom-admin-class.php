<?php

namespace StatusTheme;

use StatusTheme\SingletonOption as PingdomOpion;

defined('ABSPATH') || exit;

class PingdomAdmin extends PingdomAPI {

    public function __construct() {

        parent::__construct();

        add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );
        add_action( 'wp_ajax_save_pingdom_token', array( $this, 'save_pingdom_token' ) );
        add_action( 'wp_ajax_update_checks', array( $this, 'update_checks' ) );
        add_action( 'wp_ajax_update_check_results', array( $this, 'update_check_results' ) );
        //add_action( 'wp_ajax_update_summary', array( $this, 'update_checks_summary' ) );
        add_action( 'wp_ajax_update_outage', array( $this, 'update_checks_outage' ) );
        add_action( 'wp_ajax_update_analysis', array( $this, 'update_checks_analysis' ) );
        add_action( 'wp_ajax_create_incidents_posts', array( $this, 'create_incidents_posts' ) );
    }


    public function add_admin_menu() {
        add_menu_page('Pingdom Settings', 'Pingdom', 'manage_options', 'pingdom-settings', array( $this, 'create_admin_page' ), 'dashicons-admin-generic', 30);
    }

    public function create_admin_page() {
        ?>
        <div class="pingdom-admin-wrap">
            <h1>Pingdom Settings</h1>
            <h2 class="pingdom-admin-nav-tab-wrapper">
                <a href="?page=pingdom-settings&action=actions" class="pingdom-admin-nav-tab">Actions</a>
                <a href="?page=pingdom-settings" class="pingdom-admin-nav-tab pingdom-admin-nav-tab-active">Credentials</a>
            </h2>
            <div class="pingdom-admin-content-box">
                <div class="pingdom-admin-content-tab actions">
                    <div class="pingdom-admin-content update-checks">
                        <button class="pingdom-admin-button update-checks" data-value_action="update_checks">Update PingDom Checks</button>
                        <div class="pingdom-admin-action-result"></div>
                    </div>
                    <div class="pingdom-admin-content update-check-results">
                        <button class="pingdom-admin-button update-check-results" data-value_action="update_check_results">Update Check Results</button>
                        <div class="pingdom-admin-action-result"></div>
                    </div>
                    <!--<div class="pingdom-admin-content update-summary">
                        <button class="pingdom-admin-button update-summary" data-value_action="update_summary">Update Summary Data</button>
                        <div class="pingdom-admin-action-result"></div>
                    </div>-->
                    <div class="pingdom-admin-content update-outage">
                        <button class="pingdom-admin-button update-outage" data-value_action="update_outage">Update Outage Data</button>
                        <div class="pingdom-admin-action-result"></div>
                    </div>
                    <div class="pingdom-admin-content update-analysis">
                        <button class="pingdom-admin-button update-analysis" data-value_action="update_analysis">Update Analysis</button>
                        <div class="pingdom-admin-action-result"></div>
                    </div>
                    <div class="pingdom-admin-content create-incidents-posts">
                        <button class="pingdom-admin-button create-incidents-posts" data-value_action="create_incidents_posts">Create-Incidents-Posts</button>
                        <div class="pingdom-admin-action-result"></div>
                    </div>
                </div>
                <div class="pingdom-admin-content-tab credentials">
                    <form id="pingdom-settings-form" class="pingdom-admin-content" method="post">
                        <?php
                        $token = SingletonOption::getInstance( self::$pingdome_token_field_name );
                        ?>
                        <input type="text" id="pingdom-token" name="<?php echo esc_attr( self::$pingdome_token_field_name ); ?>" value="<?php echo esc_attr( $token ); ?>" />
                        <button class="pingdom-admin-button save-token" data-value_action="save_pingdom_token" data-value_<?php echo esc_attr( self::$pingdome_token_field_name ); ?>="<?php echo $token; ?>">Save</button>
                        <div class="pingdom-admin-action-result"></div>
                    </form>
                </div>
            </div>
        </div>
        <?php
    }

    public function save_pingdom_token() {
        $result = json_encode([ 'result' => 1, 'message' => 'not valid token' ]);
        if ( isset( $_POST[ self::$pingdome_token_field_name ] ) && ! empty( $_POST[ self::$pingdome_token_field_name ] ) ) {
            update_option( self::$pingdome_token_field_name, sanitize_text_field($_POST[ self::$pingdome_token_field_name ]));
            $result = json_encode([ 'result' => 1, 'message' => 'success' ]);
        }
        echo $result;
        wp_die();
    }


    public function update_checks_summary() {

        $checks = SingletonOption::getInstance( self::$pingdome_checks_field_name );
        if ( ! empty( $checks ) ) {
            foreach ( $checks as $check_id => $check ) {
                $check_data = self::pullCheckSummary( $check_id );
            }
        }

    }


    public function update_checks( $mode = 'ajax' ) {
        $result = json_encode([ 'result' => 0, 'message' => 'data is not updated' ]);
        $checks = self::request('GET', 'checks');
        if ( $checks && isset( $checks->checks ) ) {
            $data = [];
            foreach ( $checks->checks as $check ) {
                $data[ $check->id ] = $check;
            }
            $data['last_updated'] = time();
            update_option( self::$pingdome_checks_field_name, $data, 'no' );
            $result = json_encode([ 'result' => 1, 'message' => 'data is updated' ]);
        }
        if ( empty( $mode ) || $mode == 'ajax' ) {
            echo $result;
            wp_die();
        }
        else {
            return $data;
        }

    }


    public function update_check_results( $mode = 'ajax', $checks_ids = [] ) {

        $result = json_encode([ 'result' => 0, 'message' => 'data is not updated' ]);
        if ( empty( $checks_ids ) ) {
            $checks = SingletonOption::getInstance( self::$pingdome_checks_field_name );
            $checks_ids = array_keys( $checks );
        }
        $outages = SingletonOption::getInstance( self::$pingdome_outages_field_name, [] );
        if ( ! empty( $checks_ids ) ) {
            foreach ( $checks_ids as $check_id ) {
                $check_data = self::pullCheckResults( $check_id );
                $outages[ $check_id ] = $check_data;
            }
            $result = json_encode([ 'result' => 1, 'message' => 'data is updated' ]);
        }
        echo $result;
        wp_die();

    }


    public function update_checks_outage( $checks_ids = [], $args = [] ) {

        $result = json_encode([ 'result' => 0, 'message' => 'data is not updated' ]);
        if ( empty( $checks_ids ) ) {
            $checks = SingletonOption::getInstance( self::$pingdome_checks_field_name );
            $checks_ids = array_keys( $checks );
        }
        $outages = SingletonOption::getInstance( self::$pingdome_outages_field_name, [] );
        if ( ! empty( $checks_ids ) ) {
            foreach ( $checks_ids as $check_id ) {
                $check_data = self::pullCheckOutage( $check_id, $args );
                $outages[ $check_id ] = $check_data;
            }
            update_option( self::$pingdome_outages_field_name, $outages, 'no' );
            $result = json_encode([ 'result' => 1, 'message' => 'data is updated' ]);
        }
        echo $result;
        wp_die();

    }

    public function get_summary_check_outages( $check_id, $args = [] ) {

        $defaults = [
                'from' => strtotime('-90 days', time()),
                'to' => time()
            ];

        $args = shortcode_atts( $defaults, $args );

        if ( is_int( $check_id ) ) {
            $statuses = [ 'up' => 0, 'down' => 0, 'unknown' => 0 ];
            $outages = SingletonOption::getInstance( self::$pingdome_outages_field_name );
            if ( isset( $outages[ $check_id ] ) ) {
                foreach ( $outages[ $check_id ] as $outage ) {
                    $value = pingdomCalendar::get_diapason_intersection(
                        ['from' => $outage->timefrom, 'to'=> $outage->timeto],
                        ['from' => $args['from'], 'to'=> $args['to']] );
                    $statuses[ $outage->status ] += $value;
                }
            }
            if ( $statuses['up'] == 0 && $statuses['up'] == $statuses['down'] && $statuses['up'] == $statuses['unknown'] ) {
                $statuses['down'] = 1;
            }
            return $statuses;
        }

        return false;

    }

    public function update_checks_analysis( $checks_ids = [], $args = [] ) {

        $result = json_encode([ 'result' => 0, 'message' => 'data is not updated' ]);
        if ( empty( $checks_ids ) ) {
            $checks = SingletonOption::getInstance( self::$pingdome_checks_field_name );
            $checks_ids = array_keys( $checks );
        }
        $analysis = SingletonOption::getInstance( self::$pingdome_analysis_field_name, [] );
        if ( ! empty( $checks_ids ) ) {
            foreach ( $checks_ids as $check_id ) {
                if ( is_int( $check_id ) ) {
                    $check_analysis = self::pullCheckAnalysis( $check_id, $args );
                    $analysis[ $check_id ] = $check_analysis;
                }
            }
            update_option( self::$pingdome_analysis_field_name, $analysis, 'no' );
            $result = json_encode([ 'result' => 1, 'message' => 'data is updated' ]);
        }
        echo $result;
        wp_die();

    }


    public function get_summary_up_time( $checks, $from, $to ) {

        $up = $down = 0;
        foreach ( $checks as $check_id => $check_data ) {
            if ( is_object( $check_data ) ) {
                $summary_status = $this->get_summary_check_outages( $check_id, [ 'from' => $from, 'to' => $to ] );
                $up += $summary_status['up'];
                $down += ( $summary_status['down'] );
            }
        }

        return [ 'up' => $up, 'down' => $down ];

    }


    public function create_pt_incident_inctance( $incident, $check_id ) {

        $PingdomAdmin = Singleton::getInstance('StatusTheme\PingdomAdmin');
        $outages = PingdomOpion::getInstance( $PingdomAdmin::$pingdome_outages_field_name );
        $outage_data = pingdomCalendar::get_incident_outage( $incident, $check_id, $outages );
        $block_attributes = array(
            'incidentId' => strval($incident['id']),
            'incidentDomain' => $incident['host_name'],
            'incidentDescription' => $incident['longdesc'],
            'incidentStartDate' => $outage_data->timefrom,
            'incidentEndDate' => $outage_data->timeto,
            'incidentCustomDescription' => ''
        );
        $block_content = '<!-- wp:status/post ' . wp_json_encode( $block_attributes ) . ' --><div></div><!-- /wp:status/post -->';

        $dateFormatter = new \IntlDateFormatter('en_US', \IntlDateFormatter::LONG, \IntlDateFormatter::NONE);
        $dateFormatter->setPattern('MMMM dd y HH:mm:ss');

        $post_data = array(
            'post_title'    => $incident['host_name'] . '_' . $dateFormatter->format( new \DateTime( date( 'Y-m-d H:i:s', $outage_data->timefrom ) ) ) . '_' . $dateFormatter->format( new \DateTime( date( 'Y-m-d H:i:s', $outage_data->timeto ) ) ),
            'post_status'   => 'publish',
            'post_type'     => 'incident',
            'post_content'  => $block_content,
            'meta_input'            => array(
                'status_connected_incident' => $incident['id'],
                'status_connected_check_id' => $check_id,
            ),
        );

        $post_id = wp_insert_post( $post_data );

        return $post_id;

    }


    public function create_incidents_posts( $mode = 'ajax' ) {

        $PingdomAdmin = Singleton::getInstance('StatusTheme\PingdomAdmin');
        $analysis = PingdomOpion::getInstance($PingdomAdmin::$pingdome_analysis_field_name);
        $result = [ 'result' => 0, 'message' => 'no incidents created' ];

        $posts = [];
        foreach ( $analysis as $check_id => $analysis_data ) {
            foreach ( $analysis_data as $analis_id => $analis_data ) {
                $post = get_posts([
                    'post_type' => 'incident',
                    'post_status' => 'publish',
                    'posts_per_page' => 1,
                    'meta_query' => [
                        [
                            'key' => 'status_connected_incident',
                            'value' => $analis_id,
                            'compare' => '='
                        ]
                    ]
                ]);
                if ( count( $post ) == 0 ) {
                    $incident = pingdomCalendar::form_incident( $analis_id, $analis_data, $check_id );
                    $post_id = $this->create_pt_incident_inctance( $incident, $check_id );
                    if ( $post_id ) {
                        $posts[] = [ 'analis_id' => $analis_id, 'check_id' => $check_id ];
                    }
                }
            }
        }

        if ( empty( $mode ) || $mode == 'ajax' ) {
            if ( count( $posts ) > 0 ) {
                $posts = array_map( function( $value ) {
                    return 'analis_id=' . $value['analis_id'] . ', ' . 'check_id=' . $value['check_id'] . '<br>';
                }, $posts );
                $result = [ 'result' => 1, 'message' => implode( '', $posts ) ];
            }
            echo json_encode($result);
            wp_die();
        }
        else {
            return $posts;
        }

    }

}

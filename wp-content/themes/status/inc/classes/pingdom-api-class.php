<?php

namespace StatusTheme;

class PingdomAPI {

    protected static $pingdome_token_field_name = 'pingdom_token';
    public static $pingdome_checks_field_name = 'pingdom_checks';
    public static $pingdome_outages_field_name = 'pingdom_outages';
    public static $pingdome_analysis_field_name = 'pingdom_analysis';

    private static $token;

    public function __construct() {
        self::$token = SingletonOption::getInstance( self::$pingdome_token_field_name );
    }

    public static function request($method, $endpoint, $params = [], $data = [] ) {
        $ch = curl_init();

        $headers = [
            'Authorization: Bearer ' . self::$token,
            //'Content-Type: application/json',
        ];

        $url = 'https://api.pingdom.com/api/3.1/' . ltrim($endpoint, '/');
        if ( ! empty( $params ) ) {
            $url .= '?' . http_build_query( $params );
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if ($method === 'POST' || $method === 'PUT') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        try {
            $response = curl_exec($ch);
            $response = json_decode( $response );
        } catch( Exception $e ) {
            error_log('cURL error: ' . $e->getMessage());
            $response = null;
        }

        curl_close($ch);

        return $response;
    }

    public function pullCheckSummary( $check_id, $args = [] ) {

        $defaults = [
            'bycountry' => 'false',
            'byprobe' => 'false',
            'from' => strtotime('-30 days', time()),
            'includeuptime' => 'false',
            //'probes' => '',
            'to' => time()

        ];
        $args = shortcode_atts( $defaults, $args );

        $data = self::request('GET', 'summary.average/' . $check_id, $args );
        return $data;

    }


    public function pullCheckResults( $check_id, $args = [] ) {

        $defaults = [
            'from' => strtotime('-30 days', time()),
            'includeanalysis' => 'true',
            'limit' => 1000,
            'maxresponse' => null,
            'minresponse' => null,
            'offset' => 0,
            'probes' => null,
            'status' => null,
            'to' => time()
        ];
        $args = shortcode_atts( $defaults, $args );

        $data = self::request('GET', 'results/' . $check_id, $args );
        return $data;

    }


    public function pullCheckOutage( $check_id, $args = [] ) {

        $defaults = [
            'from' => strtotime('-365 days', time()),
            'order' => 'desc',
            'to' => time()
        ];
        $args = shortcode_atts( $defaults, $args );

        $data = self::request('GET', 'summary.outage/' . $check_id, $args );
        $data = ( isset( $data->summary ) && isset( $data->summary->states ) ) ? $data->summary->states : [];

        return $data;

    }

    public function pullCheckAnalys( $check_id, $analys_id ) {

        $data = self::request('GET', 'analysis/' . $check_id . '/' . $analys_id );
        return $data;

    }

    public function pullCheckAnalysis( $check_id, $args = [] ) {

        $defaults = [
            'from' => strtotime('-365 days', time()),
            'limit' => 100,
            'offset' => 0,
            'to' => time()

        ];
        $args = shortcode_atts( $defaults, $args );

        $data = new \stdClass();
        $data->analysis = [0];
        $analysis = SingletonOption::getInstance( self::$pingdome_analysis_field_name, [] );
        $check_analysis = isset( $analysis[ $check_id ] ) ? $analysis[ $check_id ] : [];

        while( count( $data->analysis ) > 0 ){
            $data = self::request('GET', 'analysis/' . $check_id, $args );
            foreach ( $data->analysis as $analis ) {
                if ( ! isset( $check_analysis[ $analis->id ] ) ) {
                    $analis_data = self::pullCheckAnalys( $check_id, $analis->id );
                    $check_analysis[ $analis->id ] = $analis;
                    $check_analysis[ $analis->id ]->data = $analis_data;
                }
            }
            $args['offset'] += $args['limit'];
        }

        return $check_analysis;

    }

}
<?php

namespace StatusTheme;

defined('ABSPATH') || exit;

class Singleton {

    protected static $instances = [];

    public static function getInstance( $className ) {
        if ( ! class_exists( $className ) ) {
            throw new \Exception("Class $className does not exist");
        }
        if ( ! isset( self::$instances[ $className ] ) ) {
            self::$instances[ $className ] = new $className();
        }
        return self::$instances[ $className ];
    }

}
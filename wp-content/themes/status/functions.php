<?php

namespace StatusTheme;

defined('ABSPATH') || exit;

require_once( get_template_directory() . '/inc/classes/singleton-class.php' );
require_once( get_template_directory() . '/inc/classes/singleton-option-class.php' );
require_once( get_template_directory() . '/inc/classes/status-theme-class.php' );
require_once( get_template_directory() . '/inc/classes/pingdom-api-class.php' );
require_once( get_template_directory() . '/inc/classes/pingdom-admin-class.php' );
require_once( get_template_directory() . '/inc/classes/pingdom-calendar-class.php' );
require_once( get_template_directory() . '/inc/classes/webhook-handler-class.php' );


$Status = Singleton::getInstance('StatusTheme\Status');
$PingdomAdmin = Singleton::getInstance('StatusTheme\PingdomAdmin');

add_action( 'wp_head', function() {
    echo '<meta name="robots" content="noindex,follow">';
} );